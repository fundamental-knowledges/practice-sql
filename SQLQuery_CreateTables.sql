create table KhachHang(
MaKH varchar(10) primary key,
HoTen nvarchar(50),
DiaChi nvarchar(100),
SoDT varchar(11),
NgSinh datetime,
DoanhSo int,
NgDk datetime
)
create table NhanVien(
MaNV varchar(10) primary key,
HoTen nvarchar(50),
SoDT varchar(11),
NgVaoLam datetime
)
create table SanPham(
MaSP varchar(10) primary key,
TenSP nvarchar(50),
DVT nvarchar(50),
NuocSX varchar(50),
Gia int
)
create table HoaDon(
SoHD varchar(10) primary key,
NgHD datetime,
MaKH varchar(10),
MaNV varchar(10),
TriGia int
)
create table CTHD(
SoHD varchar(10),
MaSP varchar(10),
SL int
Constraint PK_CTHD Primary Key (SoHD, MaSP)
)

insert KhachHang values('KH01','Vo Minh Quan','123/43, Pham Ngu Lao','0987394999','1999-12-20',3,'2019-08-02')
insert KhachHang values('KH02','Hoang Thi Tuyet','123/43, Pham Ngu Lao','0956333444','2000-08-02',3,'2020-05-04')
insert KhachHang values('KH03','Tran Ba Kha','123/43, Pham Ngu Lao','0786234901','1998-05-10',3,'2020-01-01')

insert NhanVien values('NV01','Dang Minh Vu','0917489333','2015-05-10')
insert NhanVien values('NV02','Lai Anh Dung','0898343545','2015-04-09')
insert NhanVien values('NV03','Dang Tan Loc','0938909357','2015-03-15')

insert SanPham values('SP01','May giat Hafa',null,'Viet Nam',4000)
insert SanPham values('SP02','Tu Lanh Neon',null,'Han Quoc',10000)
insert SanPham values('SP03','Bon cau Sini',null,'Nhat Ban',7000)
insert SanPham values('SP04','May rua chen Hafa',null,'Viet Nam',5500)
insert SanPham values('SP05','May lanh Hafa',null,'Viet Nam',6300)

insert HoaDon values('HD01','2020-12-15','KH02','NV01',8000)
insert HoaDon values('HD02','2020-09-15','KH03','NV03',7000)
insert HoaDon values('HD03','2020-10-02','KH01','NV03',11000)
insert HoaDon values('HD04','2020-10-02','KH01','NV02',6300)



insert CTHD values('HD01','SP01',2)
insert CTHD values('HD02','SP03',1)
insert CTHD values('HD01','SP04',2)
insert CTHD values('HD04','SP05',1)


