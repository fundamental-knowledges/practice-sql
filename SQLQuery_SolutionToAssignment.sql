--ex1
select sp.MaSP,sp.TenSP 
from SanPham sp 
where NuocSX='Viet Nam' 
union
select sp.MaSp,sp.TenSP
from SanPham sp
join CTHD ct on sp.MaSP=ct.MaSP 
join HoaDon hd on ct.SoHD=hd.SoHD
where NgHD='2023-05-23'
--ex2
select MaSP,TenSP 
from SanPham
where SanPham.MaSP not in (select MaSP from CTHD)
--ex3
select SoHD
from CTHD ct
join SanPham sp on ct.MaSp=sp.MaSP
group by SoHD
having count(*)=(select count(*) from SanPham where NuocSX='Viet Nam')